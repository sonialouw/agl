﻿using System;
using AGL.Common.Logging;
using AGL.Services.Abstract;
using AGL.Domain.Enums;
using AGL.Common.Extensions;
using System.Web.Mvc;
using AGL.Web.Models;
using System.Linq;

namespace AGL.Web.Controllers
{
    public class HomeController :Controller
    {
        private IPeopleService PeopleService { get; set; }
        private ILogger Logger  { get; set; }

        public HomeController(ILogger logger, IPeopleService peopleService)
        {
            Logger = logger;
            PeopleService = peopleService;
        }

        public ActionResult Index()
        {
            var viewModel = new PetViewModel();
            try
            {
                var petResult = PeopleService.GetPets();
                if (petResult.Success)
                {
                    viewModel.Pets = petResult.Data.Where(i=>i.PetType == PetTypeEnum.Cat).ToList() ;
                }
            }
            catch (Exception ex)
            {
               Logger.Error(ex.ToString());
            }

            viewModel.Genders = EnumExtensions.GetValues<GenderEnum>();
            return View(viewModel);
        }
    }
}