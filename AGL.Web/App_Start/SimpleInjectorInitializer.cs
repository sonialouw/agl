﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AGL.Common.Logging;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using AGL.Services.Abstract;
using AGL.Services.Concrete;
using AGL.External.Api.Abstract;
using AGL.External.Api.Concrete;
using System.Web.Mvc;

namespace AGL.Web.App_Start
{
    public class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as the MVC & WebApi Dependency Resolvers.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static void InitializeContainer(Container container)
        {
            container.Register<ILogger, NLogger>(Lifestyle.Scoped);
            container.Register<IExternalApi, ExternalApi>(Lifestyle.Scoped);
            container.Register<IPeopleService, PeopleService>(Lifestyle.Scoped);
        }
    }
}