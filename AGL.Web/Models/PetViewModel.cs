﻿using AGL.Common.Data;
using AGL.Domain.Entities;
using AGL.Domain.Enums;
using AGL.Services.Data.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Web.Models
{
    public class PetViewModel
    {
        public List<Pet> Pets { get; set; }
        public List<EnumValue> Genders { get; set; }
    }
}