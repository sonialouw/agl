﻿using AGL.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGL.Domain.Entities
{
    public class Pet
    {
        public string Name { get; set; }
        public PetTypeEnum PetType { get; set; }
        public Person Person { get; set; }
    }
}

