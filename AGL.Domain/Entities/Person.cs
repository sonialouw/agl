﻿using AGL.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGL.Domain.Entities
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public GenderEnum Gender { get; set; }

        public List<Pet> Pets { get; set; }
    }
}

