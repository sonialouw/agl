﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Domain.Enums
{
    public enum GenderEnum
    {
        Female = 1,
        Male = 2
    }
}
