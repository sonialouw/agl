﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Domain.Enums
{
    public enum PetTypeEnum
    {
        Cat = 1,
        Dog = 2,
        Fish = 3
    }
}
