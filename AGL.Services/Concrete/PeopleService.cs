﻿using AGL.External.Api.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGL.Domain.Entities;
using AGL.Services.Abstract;
using AGL.Services.Data.Result;
using AGL.Domain.Enums;
using AGL.Common.Extensions;
using AGL.Common.Logging;

namespace AGL.Services.Concrete
{
    public class PeopleService : BaseService, IPeopleService
    {
        private IExternalApi ExternalApi;

        public PeopleService(ILogger logger, IExternalApi externalApi) : base(logger)
        {
            ExternalApi = externalApi;
        }

        public PeopleResult GetPeople()
        {
            var result = new PeopleResult();

            try
            {
                var response = ExternalApi.GetPeople();
                if (response.Success)
                {
                    var people = response.People.Select(i => new Person()
                    {
                        FirstName = i.Name,
                        Age = i.Age,
                        Gender = i.Gender.ToEnum<GenderEnum>(),
                        Pets = i.Pets?.Select(p => new Pet()
                        {
                            Name = p.Name,
                            PetType = p.Type.ToEnum<PetTypeEnum>(),
                        }
                        )
                        .ToList()
                    })
                    .ToList();

                    result.Data = people;
                    result.Success = true;
                }
                else
                {
                    result.Message = result.Message;
                    result.Errors = result.Errors.Select(i => new BaseResultError(i.Key, i.Message)).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                result.Message = "An unexpected error occured.";
            }

            return result;
        }

        public PetResult GetPets()
        {
            var result = new PetResult();

            try
            {
                var response = ExternalApi.GetPeople();
                if (response.Success)
                {
                    var pets = response.People
                        .Where(i => i.Pets != null)
                        .SelectMany(
                            p => p.Pets.Select(i =>
                            new Pet()
                            {
                                Name = i.Name,
                                PetType = i.Type.ToEnum<PetTypeEnum>(),
                                Person = new Person()
                                {
                                    FirstName = p.Name,
                                    Age = p.Age,
                                    Gender = p.Gender.ToEnum<GenderEnum>(),
                                }
                            })).ToList();

                    result.Data = pets;
                    result.Success = true;
                }
                else
                {
                    result.Message = result.Message;
                    result.Errors = result.Errors.Select(i => new BaseResultError(i.Key, i.Message)).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                result.Message = "An unexpected error occured.";
            }

            return result;
        }

    }
}
