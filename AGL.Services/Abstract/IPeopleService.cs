﻿using AGL.Services.Data.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Services.Abstract
{
    public interface IPeopleService
    {
        PeopleResult GetPeople();
        PetResult GetPets();
    }
}
