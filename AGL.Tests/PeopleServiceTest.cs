﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace AGL.Tests
{
    [TestClass]
    public class PeopleServiceTest : IntegrationTestBase
    {
        [TestMethod]
        public void GetPeople()
        {
            var result = PeopleService.GetPeople();
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Data.Any());
        }

        [TestMethod]
        public void GetPets()
        {
            var result = PeopleService.GetPets();
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Data.Any());
        }

    }
}
