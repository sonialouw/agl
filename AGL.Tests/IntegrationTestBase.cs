﻿using AGL.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AGL.Services.Concrete;
using AGL.Common.Logging;
using AGL.External.Api.Abstract;
using AGL.External.Api.Concrete;

namespace AGL.Tests
{
    public abstract class IntegrationTestBase
    {
        protected IPeopleService PeopleService { get; set; }
        protected ILogger Logger { get; set; }
        protected IExternalApi ExternalApi { get; set; }

        protected virtual void Initialize() { }
        protected virtual void CleanUp() { }

        
        [TestInitialize]
        public void TestSetup()
        {
            Logger = new NLogger();
            ExternalApi = new ExternalApi();
            PeopleService = new PeopleService(Logger, ExternalApi);
            Initialize();
        }

        [TestCleanup]
        public void TestCleanUp()
        {           
            CleanUp();
        }
    }
}
