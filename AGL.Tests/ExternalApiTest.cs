﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace AGL.Tests
{
    [TestClass]
    public class ExternalApiTest : IntegrationTestBase
    {

        [TestMethod]
        public void GetPeople()
        {
            var result = ExternalApi.GetPeople();
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(result.People);
            Assert.IsTrue(result.People.Any());
        }

    }
}
