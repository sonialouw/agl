﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Services.Data.Dto
{
    public class Pet
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
