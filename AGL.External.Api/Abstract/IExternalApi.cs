﻿
using AGL.External.Api.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.External.Api.Abstract
{
    public interface IExternalApi
    {
        PeopleResponse GetPeople();
    }
}
