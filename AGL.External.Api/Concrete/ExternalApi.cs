﻿using AGL.External.Api.Abstract;
using System;
using System.Net.Http;
using AGL.External.Api.Util;
using System.Collections.Generic;
using AGL.External.Api.Response;
using AGL.Services.Data.Dto;
using RestSharp;

namespace AGL.External.Api.Concrete
{
    public class ExternalApi : IExternalApi
    {
        private string ExternalApiUrl => Utils.AppSetting("ExternalApiUrl");
        public PeopleResponse GetPeople()
        {
            var response = new PeopleResponse { People = new List<Person>() };

            try
            {
                var client = GetClient();
                var request = new RestRequest("/people.json");
                response.People = client.Execute<List<Person>>(request).Data;
                response.Success = true;
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        private RestClient GetClient()
        {
            var client = new RestClient() { BaseUrl = new Uri(ExternalApiUrl) };
            client.AddDefaultHeader("content-type", "application/json");
            return client;
        }


    }
}
