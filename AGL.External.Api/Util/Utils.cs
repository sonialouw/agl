﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.External.Api.Util
{
    public static class Utils
    {
        public static int AppSetting(string key, int defaultValue)
        {
            if (!int.TryParse(ConfigurationManager.AppSettings[key], out var value))
            {
                value = defaultValue;
            }
            return value;
        }

        public static bool AppSetting(string key, bool defaultValue)
        {
            if (!bool.TryParse(ConfigurationManager.AppSettings[key], out var value))
            {
                value = defaultValue;
            }
            return value;
        }

        public static string AppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static TEnum AppSetting<TEnum>(string key, TEnum defaultValue) where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            TEnum val;
            if (!Enum.TryParse(ConfigurationManager.AppSettings[key], true, out val))
            {
                val = defaultValue;
            }

            return val;
        }
    }
}
