﻿using AGL.Services.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGL.External.Api.Response;

namespace AGL.External.Api.Response
{
    public class PeopleResponse : BaseResponse
    {
        public ICollection<Person> People { get; set; }
    }
}
