﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Common.Data
{
    public class EnumValue
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
