﻿using System;
using NLog;
using NLog.Config;
using AGL.Common.Extensions;

namespace AGL.Common.Logging
{
    public class NLogger : ILogger
    {
        private Logger Logger { get; set; }

        public NLogger()
        {
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("utc_date", typeof(UtcDateRenderer));
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("web_variables", typeof(WebVariablesRenderer));

            Logger = LogManager.GetCurrentClassLogger();
        }

        public void Info(string message)
        {
            Logger.Info(message);
        }

        public void Warn(string message)
        {
            Logger.Warn(message);
        }

        public void Debug(string message)
        {
            Logger.Debug(message);
        }

        public void Error(string message)
        {
            Logger.Error(message);
        }

        public void Error(Exception x)
        {
            Error(x.BuildExceptionMessage());
        }

        public void Error(string message, Exception x)
        {
            Logger.Error(x, message);
        }

        public void Fatal(string message)
        {
            Logger.Fatal(message);
        }

        public void Fatal(Exception x)
        {
            Fatal(x.BuildExceptionMessage());
        }
    }
}
