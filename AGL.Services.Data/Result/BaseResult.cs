﻿using AGL.Services.Data.Response;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AGL.Services.Data.Result
{

    public class BaseResult<TData>
    {
        public bool Success { get; set; }
        public TData Data { get; set; }
        public List<BaseResultError> Errors { get; set; }
        public string Message { get; set; }

        public BaseResult()
        {
            Errors = new List<BaseResultError>();
        }

        public BaseResult(bool success, string message, TData data) : this()
        {
            this.Success = success;
            this.Data = data;
            this.Message = message;
        }

        public BaseResult(bool success, string message) : this()
        {
            this.Success = success;
            this.Message = message;
        }

        public static BaseResult<TData> WithoutData(bool success, string message = "")
        {
            return new BaseResult<TData>(success, message);
        }
    }

   
}
