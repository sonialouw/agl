﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Services.Data.Result
{
    public class BaseResultError
    {
        public string Key { get; set; }
        public string Message { get; set; }



        public BaseResultError(string key, string message)
        {
            Key = key;
            Message = message;
        }
    }
}
